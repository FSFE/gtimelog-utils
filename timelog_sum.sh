#!/usr/bin/env bash

set -e
set -o pipefail

TIMELOG_TXT="/home/$USER/.local/share/gtimelog/timelog.txt"

read -p "If timelog.txt location is different than [$TIMELOG_TXT] please enter path:" timelog_txt
timelog_txt=${timelog_txt:-$TIMELOG_TXT}
read -p "Start time (YYYY-MM-DD) inclusive date: " time_start
read -p "End time (YYYY-MM-DD) exclusive date: " time_end
read -p "Task you want to search for: " task_var

dos2unix < "$timelog_txt" | \
awk -v human_time_start="$time_start" -v human_time_end="$time_end" '
#If the whole line matches time_start the awk script returns the timeframe. As soon as time_end is reached it stops.

# mktime_timelog converts a YYYY-MM-DD timestring to its Unix timestamp.
function mktime_timelog(ts) {
  ts_clean = gensub(/[-: ]/, " ", "g", ts)
  return mktime(ts_clean " 00 00 00")  # add trailing HH MM SS
}

BEGIN {
  time_start = mktime_timelog(human_time_start)
  time_end = mktime_timelog(human_time_end)
}

mktime_timelog($1) >= time_end {
  exit 0
}

mktime_timelog($1) >= time_start  {
  print
}
' |\
awk -f timelog_sum.awk -v search="$task_var"
