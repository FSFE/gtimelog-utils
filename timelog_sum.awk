#general commands for running the script for a specif date with a specific day:
#awk -f timelog_sum.awk -v 'search=TASK' /home/$Username/.local/share/gtimelog/timelog.txt
#command for a month:
#grep '^2022-11' /home/$Username/.local/share/gtimelog/timelog.txt |awk -f timelog_sum.awk -v 'search=TASL' 
#command for a certain timeframe
#egrep '^(2022-1[12]|2023-(0.|10))' /home/$Username/.local/share/gtimelog/timelog.txt |awk -f timelog_sum.awk -v search='TASK' (everything from 2022 11 (Nov) and 12 (Dec) and 2023 every month with a starting 0 or the month 10)
#


function mktime_timelog(ts) {
  ts_clean = gensub(/[-: ]/, " ", "g", ts)
  return mktime(ts_clean " 00")
}

function time_fmt(ts) {
  ts = int(ts / 60)  # original time misses seconds

  m = ts % 60; ts = int(ts / 60)
  # h = ts % 24; ts = int(ts / 24)
  # d = ts #for days remove line below, and add this line and line above. 
  h = ts

  # return sprintf("%dd%02dh%02dm", d, h, m) # for days remove line below and add this line (reomve # to add)
  return sprintf("%02dh%02dm", h, m)
}

BEGIN {
  FS = ": "
  last_date = "0000-00-00 00:00"
  total = 0
}

/^$/ { next }

$0 !~ /^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:/ {
  print "WARN: Invalid input in line " NR ": " $0
  next
}

($0 ~ search) {
  duration = mktime_timelog($1) - last_date
  total += duration
}

/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}: / {
  last_date = mktime_timelog($1)
}

END {
  print "Total: " time_fmt(total)
}
