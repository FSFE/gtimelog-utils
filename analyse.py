# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import datetime
import sys
import argparse
import csv
import configparser
import fileinput
from helpers.table import print_table
from helpers.gtimelog import (
    parse_datetime,
    format_duration_short,
    as_minutes,
    remove_tags
)

Config = configparser.ConfigParser()
Config.read("config.ini")

parser = argparse.ArgumentParser(description='Analyse gtimelogs')
parser.add_argument("-f", "--file", dest="filename", required=True,
                    help="the FILE to analyse. Use '-' to read from stdin")
parser.add_argument("-r", "--reduce-to", dest="reduce",
                    choices=["categories", "supercategories"],
                    help="Reduce activities to (super)categories")
parser.add_argument("-s", "--style", dest="style",
                    choices=["csv", "pretty"], default="pretty",
                    help="Define output style")
parser.add_argument("-t", "--time-format", dest="timeformat",
                    choices=["hm", "m"], default="hm",
                    help="Set time format to HH:MM (default) or minutes")
parser.add_argument("-a", "--all", action="store_true",
                    help="Do not ignore categories set in config.ini")

args = parser.parse_args()

# Split ignored categories from config, strip them, and make them a tuple
ignored_categories = tuple(
    [x.strip() for x in Config.get("General", "ignored_categories").split(',')]
    )

# HELPER FUNCTIONS
def split_activities(file):
    """Split complete timelog by lines, so every line is a separate entry"""
    items = []
    try:
        for line in fileinput.input(file):
            # Separate timestamp and actual title of the entry
            # Note: Very important that format is respected (timestamp and task
            # separated by ': ')
            time, sep, title = line.partition(': ')
            # Excludes comments, empty lines etc
            if not sep:
                continue
            try:
                time = parse_datetime(time)
            except ValueError:
                continue
            # Remove tags and trailing whitespaces from activity title
            title = remove_tags(title)

            # Modify the title based on reduction choice:
            # default: all activities in their full length, except tag (no modification)
            # categories: split at first :
            # supercategories: like categories, but then also split at first space
            if args.reduce in ("categories", "supercategories"):
                # Do not reduce OFF activities, they will be handled later
                if title.startswith("OFF"):
                    pass
                # Do not reduce slacking times, also "arrived **"
                elif '**' in title:
                    pass
                # Do not reduce ignored categories if not manually unignored with -a
                elif title.startswith(ignored_categories) and not args.all:
                    pass
                # All other, split into category, or even supercategory
                else:
                    cat, sep, _ = title.partition(':')
                    if sep:
                        title = cat
                    if args.reduce == "supercategories":
                        cat, sep, _ = title.partition(' ')
                        if sep:
                            title = cat

            items.append((time, title))

        return items
    except FileNotFoundError:
        sys.exit(f"File {file} not found.")

def format_durations(dur, total_dur, dur_format):
    """Format time depending on choice, and calculate percentage"""
    # Calculate proportion of this activity's duration in percent
    per = round(as_minutes(dur) / total_dur * 100, 2)
    # define duration format (HH:MM or minutes)
    if dur_format == "hm":
        dur = format_duration_short(dur)
    else:
        dur = as_minutes(dur)

    return dur, per


# ACTUAL WORK

# Create a dictionary that lists all titles and their respective total time
# Structure: {'title': ('title', 'duration'), ...}
tasks = {}

# Initialise rolling durations for off time, the ignored categories, and the
# total duration
total_duration = datetime.timedelta(0)
off_sick = off_child_sick = off_vacation = off_holiday = ignored = datetime.timedelta(0)

stop = None
for item in split_activities(args.filename):
    start = stop
    stop = item[0]  # timestamp
    title = item[1] # title
    if start is None:
        start = stop

    if start > stop:
        print(f"WARNING: negative duration from {start} to {stop}")

    duration = stop - start

    # skip slacking times, also arrived**
    if '**' in title:
        continue

    # check for ignored but still counted categories
    if "OFF Sick" in title:
        off_sick += duration
        continue
    if "OFF Child Sick" in title:
        off_child_sick += duration
        continue
    if "OFF Vacation" in title:
        off_vacation += duration
        continue
    if "OFF PublicHoliday" in title:
        off_holiday += duration
        continue

    # skip ignored categories defined in config.ini unless -a/--all
    if not args.all:
        if title.startswith(ignored_categories):
            ignored += duration
            continue

    # Search for entry for this activity in tasks.
    # If found, add duration. If not, create it
    if title not in tasks:
        tasks[title] = (title, duration)
    else:
        duration = tasks[title][1] + duration
        tasks[title] = (title, duration)

# Calculate total duration of all tasks
for v in tasks.values():
    total_duration += v[1]

# Convert total duration to minutes to ease percentage calculation
total_duration_min = as_minutes(total_duration)

# Create a list filled with dictionaries per activity summary
# Will later be converted to pretty format or CSV
activity_output = []
stats_output = []

# Prepare output
activity_header = ["Activity",
                   "Duration",
                   "Percentage"
                  ]
for task in sorted(tasks.values()):
    entry = task[0]
    duration = task[1]
    duration, percentage = format_durations(duration,
                                            total_duration_min,
                                            args.timeformat)
    activity_output.append({"Activity": entry,
                            "Duration": duration,
                            "Percentage": percentage}
                           )

# Prepare statistics
stats = [
    ("Working time", total_duration),
    ("Sick time", off_sick),
    ("Child sick time", off_child_sick),
    ("Vacation time", off_vacation),
    ("Public holidays", off_holiday),
    ("Ignored Categories", ignored)
]
for stat in stats:
    duration, _ = format_durations(stat[1],
                                   1,
                                   args.timeformat)
    stats_output.append({"Statistic": stat[0],
                         "Duration": duration})

# Print output
if args.style == "csv":
    writer = csv.DictWriter(sys.stdout, fieldnames=activity_header)
    writer.writeheader()
    for line in activity_output:
        writer.writerow(line)
else:
    print_table(activity_output)
    print()
    print_table(stats_output)
