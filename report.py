# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""
Script to generate a report for the budgeting process.
Basically, the analyse.py script with extended options
"""
import datetime
import sys
import argparse
import configparser
import fileinput
from functools import reduce
import pandas as pd
from helpers.table import print_table
from helpers.gtimelog import (
    parse_datetime,
    format_duration_short,
    as_minutes,
    remove_tags
)
def format_timedelta(td):
    return td.strftime('%H:%M')
Config = configparser.ConfigParser()
Config.read("config.ini")

parser = argparse.ArgumentParser(description='Analyse gtimelogs')
parser.add_argument("-f", "--file", dest="filename", required=True, nargs='+',
                    help="the FILE to analyse. Use '-' to read from stdin. You can use more then one filename.")
parser.add_argument("-r", "--reduce-to", dest="reduce",
                    choices=["categories", "supercategories"],
                    help="Reduce activities to (super)categories")
parser.add_argument("-s", "--style", dest="style",
                    choices=["csv", "pretty", "calc"], default="pretty",
                    help="Define output style")
parser.add_argument("-o", "--output", default="report",
                    help="Output filename (for style calc) or file prefix (for style csv)")
parser.add_argument("-t", "--time-format", dest="timeformat",
                    choices=["hm", "m"], default="hm",
                    help="Set time format to HH:MM (default) or minutes")
parser.add_argument("-a", "--all", action="store_true",
                    help="Do not ignore categories set in config.ini")
parser.add_argument("-b", "--start-date", dest="datestart", help="Start date for the report. format YYYY-MM-DD")
parser.add_argument("-e", "--end-date", dest="dateend", help="End date for the report. format YYYY-MM-DD")
parser.add_argument("-l", "--limit", action="store_true",
                    help="Limit the report by dates. If this flag is not used, then the entire timelog will be analyzed. Use the -b and -e options to set the start and end dates for the report.")
args = parser.parse_args()

if args.limit:
    if (args.datestart is None) or (args.dateend is None):
        print('Use -b and -e to set the limiting dates for the report.')
        exit(1)
    try:
        start_date = datetime.datetime.strptime(args.datestart, "%Y-%m-%d")
        end_date = datetime.datetime.strptime(args.dateend, "%Y-%m-%d")
        if start_date == end_date:
            print("Limiting the report to the same start and end date makes no sense.")
            exit(1)
        if start_date > end_date:
            print("The time frame for the report cannot end before it starts.")
            exit(1)
    except ValueError:
        print('The format for the date limits has to be YYYY-MM-DD')
        exit(1)

# Split ignored categories from config, strip them, and make them a tuple
ignored_categories = tuple(
    [x.strip() for x in Config.get("General", "ignored_categories").split(',')]
    )

# HELPER FUNCTIONS
def split_activities(file):
    """Split complete timelog by lines, so every line is a separate entry"""
    items = []
    try:
        for line in fileinput.input(file):
            line = line.strip("\n")
            # Separate timestamp and actual title of the entry
            # Note: Very important that format is respected (timestamp and task
            # separated by ': ')
            time, sep, title = line.partition(': ')
            # Excludes comments, empty lines etc
            if not sep:
                continue
            try:
                time = parse_datetime(time)
            except ValueError:
                continue
            # Remove tags and trailing whitespaces from activity title
            title = remove_tags(title)

            # Modify the title based on reduction choice:
            # default: all activities in their full length, except tag (no modification)
            # categories: split at first :
            # supercategories: like categories, but then also split at first space
            if args.reduce in ("categories", "supercategories"):
                # Do not reduce OFF activities, they will be handled later
                if title.startswith("OFF"):
                    pass
                # Do not reduce slacking times, also "arrived **"
                elif '**' in title:
                    pass
                # Do not reduce ignored categories if not manually unignored with -a
                elif title.startswith(ignored_categories) and not args.all:
                    pass
                # All other, split into category, or even supercategory
                else:
                    cat, sep, _ = title.partition(':')
                    if sep:
                        title = cat
                    if args.reduce == "supercategories":
                        cat, sep, _ = title.partition(' ')
                        if sep:
                            title = cat

            items.append((time, title))

        return items
    except FileNotFoundError:
        sys.exit(f"File {file} not found.")

def format_durations(dur, total_dur, dur_format):
    """Format time depending on choice, and calculate percentage"""
    # Calculate proportion of this activity's duration in percent
    per = round(as_minutes(dur) / total_dur * 100, 2)
    # define duration format (HH:MM or minutes)
    if dur_format == "hm":
        dur = format_duration_short(dur)
    else:
        dur = as_minutes(dur)

    return dur, per

# ACTUAL WORK
activity_header = ["Activity",
                "Duration",
                "Percentage"
                ]

def analyse_person(thefilename):
    if args.limit:
        start_date = datetime.datetime.strptime(args.datestart, "%Y-%m-%d")
        end_date = datetime.datetime.strptime(args.dateend, "%Y-%m-%d")
    else:
        start_date = datetime.datetime.strptime("0001-01-01", "%Y-%m-%d")
        end_date = datetime.datetime.strptime("9999-12-31", "%Y-%m-%d")
    # Create a dictionary that lists all titles and their respective total time
    # Structure: {'title': ('title', 'duration'), ...}
    tasks = {}
    ignored_tasks = {}

    # Initialise rolling durations for off time, the ignored categories, and the
    # total duration
    total_duration = project_time = non_project_time = datetime.timedelta(0)
    off_sick = off_child_sick = off_vacation = off_holiday = ignored = datetime.timedelta(0)

    stop = None
    for item in split_activities(thefilename):
        start = stop
        stop = item[0]  # timestamp
        if (not args.limit) or ((stop > start_date) and (stop < end_date)):
            title = item[1] # title
            if start is None:
                start = stop

            if start > stop:
                print(f"WARNING: negative duration from {start} to {stop} in {filename}")

            duration = stop - start

            # skip slacking times, also arrived**
            if '**' in title:
                continue

            # check for ignored but still counted categories
            if "OFF Sick" in title:
                off_sick += duration
                continue
            if "OFF Child Sick" in title:
                off_child_sick += duration
                continue
            if "OFF Vacation" in title:
                off_vacation += duration
                continue
            if "OFF PublicHoliday" in title:
                off_holiday += duration
                continue

            # skip ignored categories defined in config.ini unless -a/--all
            if not args.all:
                if title.startswith(ignored_categories):
                    non_project_time += duration
                    if title not in ignored_tasks:
                        ignored_tasks[title] = (title, duration)
                    else:
                        duration = ignored_tasks[title][1] + duration
                        ignored_tasks[title] = (title, duration)
                    continue

            # Search for entry for this activity in tasks.
            # If found, add duration. If not, create it
            if title not in tasks:
                tasks[title] = (title, duration)
            else:
                duration = tasks[title][1] + duration
                tasks[title] = (title, duration)

    # Calculate total duration of all tasks
    for value in tasks.values():
        project_time += value[1]

    # Convert total duration to minutes to ease percentage calculation
    project_time_min = as_minutes(project_time)

    # Create a list filled with dictionaries per activity summary
    # Will later be converted to pretty format or CSV
    activity_output = []
    ignored_activity_output = []
    stats_output = {}

    # Prepare output
    for task in sorted(tasks.values()):
        entry = task[0]
        duration = task[1]
        duration1, percentage = format_durations(duration,
                                                project_time_min,
                                                args.timeformat)
        activity_output.append({activity_header[0]: entry,
                                activity_header[1]: duration,
                                activity_header[2]: percentage}
                            )

    for task in sorted(ignored_tasks.values()):
        entry = task[0]
        duration = task[1]
        duration1, percentage = format_durations(duration,
                                                project_time_min,
                                                args.timeformat)
        ignored_activity_output.append({activity_header[0]: entry,
                                activity_header[1]: duration,
                                activity_header[2]: percentage}
                            )

    # Prepare statistics
    working_time = project_time + non_project_time
    total_duration = working_time + off_sick + off_child_sick + off_holiday + off_vacation
    stats = [
        ("Project Time", project_time),
        ("Non-Projects Time", non_project_time),
        ("Working Time", working_time),
        ("Sick time", off_sick),
        ("Child sick time", off_child_sick),
        ("Vacation time", off_vacation),
        ("Public holidays", off_holiday),
        ("Total Time", total_duration)
    ]
    for stat in stats:
        duration, _ = format_durations(stat[1],
                                    1,
                                    args.timeformat)
        stats_output[stat[0]] = stat[1]
        # stats_output[stat[0]] = duration

    return stats_output, activity_output, ignored_activity_output

# Collect all the analyses of the single gtimelog files
# in a pandas dataframe and do some basic manipulation.

collection_stat = []
collection_activities = []
collection_ignored_activities = []
names = []

# each of the gtimelog files fill be read in and analysed
# if the filename contains a "_" assume the format something_NAME.txt and use the NAME
# as ID of the columns / sheets of the output. Else count the nameless persons.

for idx, filename in enumerate(args.filename):
    try:
        name = filename.strip('.txt').split('_')[1]
    except IndexError:
        name = "person{:02d}".format(idx)
    stat, activity_output, ignored_activity_output = analyse_person(filename)

    # check if the person worked in the time frame
    if stat['Working Time']==datetime.timedelta(0):
        print("Work Time is 0, ignoring the person")
    else:
        names.append(name)

        # for each person create two dataframes, holding the stats and the activities
        # that the analyse.py script would produce per timelog. The NAME will be used
        # for column titles
        df_stat = pd.DataFrame(stat.values(), index=stat.keys())
        df_stat.columns = [name]
        df_activity = pd.DataFrame(activity_output)
        df_activity.set_index('Activity')
        df_activity.columns = ['Activity', 'Duration ({})'.format(name),'Percentage ({})'.format(name)]
        df_ignored_activity = pd.DataFrame(ignored_activity_output)
        df_ignored_activity.columns = ['Activity', 'Duration ({})'.format(name),'Percentage ({})'.format(name)]

        # collect all the activities and stats
        collection_activities.append(df_activity)
        collection_ignored_activities.append(df_ignored_activity)
        collection_stat.append(df_stat)

# create a dataframe with the collected stats
# merge them so that each gtimelog file is listed in a column
if len(collection_stat)==0:
    print("No data found. Aborting.")
    exit(0)

df_merged_stat = pd.concat(collection_stat, join='outer', axis=1)

# create a datafrane with the collected activities duration and percentage values
# merge them so that each gtimelog file is listed in 2 columns. The actual activity
# will be used as index for the dataframe.
df_merged_activity = reduce(
    lambda  left,right: pd.merge(left,right,on=['Activity'], how='outer'), collection_activities)
df_merged_ignored_activity = reduce(
    lambda  left,right: pd.merge(left,right,on=['Activity'], how='outer'), collection_ignored_activities)

df_merged_activity.set_index('Activity')
df_merged_ignored_activity.set_index('Activity')

# due to the merge of the sigular tables NaN values may be added (e.g. when one gtimelog file is missing
# categories other files do have e.g. when that person did not work on that category). For later evaluation
# of the dataframe, these NaN values are set to 0
df_merged_activity.loc[
    0:, list(df_merged_activity.filter(regex='Duration'))].fillna(pd.to_timedelta(0), inplace=True)
df_merged_activity.loc[
    0:, list(df_merged_activity.filter(regex='Percentage'))].fillna(0.0, inplace=True)

# Insert a "Total Time" column at the beginning of the dataframe and initialize the value with a timedelta of 0
df_merged_activity.insert(1,'Total Time', pd.to_timedelta(0))

# Calculate the "Total Time" as sum of all the "Duration" values per activity
df_merged_activity['Total Time'] = df_merged_activity.loc[
    0:, list(df_merged_activity.filter(regex='Duration'))].sum(axis=1)

# Convert the timedelta values in all dataframes into the desired format
timedelta_columns = df_merged_activity.select_dtypes(include=['timedelta'])
timedelta_ignored_columns = df_merged_ignored_activity.select_dtypes(include=['timedelta'])
if args.timeformat == "hm":
    # for the merged activity dataframe, these are the above found columns of type timedelta
    for col in timedelta_columns.columns:
        df_merged_activity[col] = df_merged_activity[col].apply(lambda x: format_duration_short(x))
    for col in timedelta_ignored_columns.columns:
        df_merged_ignored_activity[col] = df_merged_ignored_activity[col].apply(lambda x: format_duration_short(x))

    # for the stats, all columns
    for col in df_merged_stat.columns:
        df_merged_stat[col] = df_merged_stat[col].apply(lambda x: format_duration_short(x))

    # and then for all single sheets of timelog files, the 1st column of the stats and the 2nd of the activities
    for idx, page in enumerate(collection_stat):
        collection_stat[idx][collection_stat[idx].columns[0]] = collection_stat[idx][
            collection_stat[idx].columns[0]].apply(lambda x: format_duration_short(x))
else:
    for col in timedelta_columns.columns:
        df_merged_activity[col] = df_merged_activity[col].apply(lambda x: as_minutes(x))
    for col in timedelta_ignored_columns.columns:
        df_merged_ignored_activity[col] = df_merged_ignored_activity[col].apply(lambda x: format_duration_short(x))
    for col in df_merged_stat.columns:
        df_merged_stat[col] = df_merged_stat[col].apply(lambda x: as_minutes(x))
    for idx, page in enumerate(collection_stat):
        collection_stat[idx][collection_stat[idx].columns[0]] = collection_stat[idx][
            collection_stat[idx].columns[0]].apply(lambda x: as_minutes(x))

if args.style=="calc":
    # export the report as LibreOffice ODS file
    # one sheet per person report and two additional sheets for the overview
    try:
        writer = pd.ExcelWriter("{}.ods".format(args.output))
    except ValueError:
        writer = pd.ExcelWriter("{}.xls".format(args.output), cell_overwrite_ok=True)
    workbook=writer.book

    # A sheet for the total stats
    df_merged_stat.to_excel(writer, sheet_name='Stats Total', startrow=0, startcol=0)

    # A sheet for the activity summary showing the durations of all activities
    df_merged_activity.loc[
        :, ~df_merged_activity.columns.isin(
            list(df_merged_activity.filter(regex='Percentage')))].to_excel(
        writer,
        sheet_name='Project Time',
        startrow=0,
        startcol=0,
        index=False)

    # A sheet for the activity summary, showing the percentages
    df_merged_activity.loc[
        :, ~df_merged_activity.columns.isin(
            list(df_merged_activity.filter(regex='Duration')))].to_excel(
        writer,
        sheet_name='Project Time Percentage',
        startrow=0,
        startcol=0,
        index=False)

    # A sheet for the ignored activity summary showing the durations of all activities
    df_merged_ignored_activity.loc[
        :, ~df_merged_ignored_activity.columns.isin(
            list(df_merged_ignored_activity.filter(regex='Percentage')))].to_excel(
        writer,
        sheet_name='Non-Project Time',
        startrow=0,
        startcol=0,
        index=False)

    # A sheet for the ignored activity summary, showing the percentages
    df_merged_ignored_activity.loc[
        :, ~df_merged_ignored_activity.columns.isin(
            list(df_merged_ignored_activity.filter(regex='Duration')))].to_excel(
        writer,
        sheet_name='Non-Project Time Percentage',
        startrow=0,
        startcol=0,
        index=False)

    # And then one sheet per gtimelog file that contains the stats and the activities from that file
    for idx, page in enumerate(collection_stat):
        collection_stat[idx].to_excel(
            writer,
            sheet_name=names[idx],
            startrow=0,
            startcol=0,
            index=True)
        collection_activities[idx].to_excel(
            writer,
            sheet_name=names[idx],
            startrow=2,
            startcol=0,
            # Excel might need this, LO does not want it
            # cell_overwrite_ok=True,
            index=False)

    # close the writer and flush the file to the disk
    writer.close()
elif args.style=="csv":
    # export the report to multible CSV files, per person and the overview
    df_merged_activity.to_csv(args.output+'_activities_total.csv')
    df_merged_stat.to_csv('_stats_total.csv')
    for idx, page in enumerate(collection_stat):
        collection_stat[idx].to_csv(
            "{}_{}_stats.csv".format(args.output, names[idx]),
            index=True)
        collection_activities[idx].to_csv(
            "{}_{}_activities.csv".format(args.output, names[idx]),
            index=False)
else:
    # print an overview of the report to the terminal
    print(df_merged_stat)
    print(df_merged_activity.replace('NaN', 0).loc[
        :, ~df_merged_activity.columns.isin(
            list(df_merged_activity.filter(regex='Percentage')))])

