#!/bin/bash

# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

# Use the check.py python script to check the gtimelog log file
# and report errors to the user directly with the Gnome desktop
# notification.
# It also copies the user's local 'timelog.txt' file to the Nextcloud subfolder.
#
# This shell script should be called hourly via cron.

USERNAME=$(whoami)
TIMELOGFILE="/home/$USERNAME/.local/share/gtimelog/timelog.txt"
# In some debian versions timelog has an alternative path
ALTPATH="/home/$USERNAME/.gtimelog/timelog.txt"
TMPFILE="/tmp/$USERNAME_gtimelog_check.txt"
SCRIPTFILE="check.py"
SCRIPTDIR="/home/$USERNAME/gtimelog-utils"
ACTIVITYFILE="/home/$USERNAME/.fsfe-activities/activities.csv"
CURRENT_YEAR=$(date +"%Y")
PREVIOUS_YEAR=$(date -d "1 year ago" +"%Y")

# Check whether the user is logged-in
while [ -z "$(pgrep gnome-session -n -U $UID)" ]; do sleep 3; done

# Export the current desktop session environment variables
# Sometimes it try to change $UID so we must remove -e option or use 2>/dev/null
export $(xargs -0 -a "/proc/$(pgrep gnome-session -n -U $UID)/environ") 2>/dev/null

# Execute the input command
nohup "$@" >/dev/null 2>&1 &

if [ ! -f "$ACTIVITYFILE" ]; then
  echo "Error: activites.csv not found." >>"$TMPFILE"
fi

# Check if the timelog.txt exists
if [ ! -f  "$TIMELOGFILE" ]; then
    # Check the alternative path
    if [ -f "$ALTPATH" ]; then
        TIMELOGFILE="$ALTPATH"
    else
        echo -e "Error: timelog.txt was not found! \nCheck one of the paths: ("$TIMELOGFILE" or "$ALTPATH")"
        exit 1
    fi
fi

# Insert Instruction comment
if ! grep -q "# Please update \"Username\", \"Weekly working hours\" and \"vacation days\" fields"  "$TIMELOGFILE"; then
    sed -i '1i\
# Please update "Username", "Weekly working hours" and "vacation days" fields\
# "vacation days remaining" fields will be updated automatically'  "$TIMELOGFILE"
fi

# Insert the "username" comment if it is missing
if ! grep -q "# Username:"  "$TIMELOGFILE"; then
    sed -i '3i\
# Username: your_FSFE_username \
# ----------------------------------------------------------------------'  "$TIMELOGFILE"
fi

# Insert the ""$PREVIOUS_YEAR" - vacation days" comment if it is missing
if ! grep -q "# "$PREVIOUS_YEAR" - vacation days:"  "$TIMELOGFILE"; then
    sed -i "/# Username:/a # "$PREVIOUS_YEAR" - vacation days: 0"  "$TIMELOGFILE"
    echo "Error: Please update # "$PREVIOUS_YEAR" - vacation days in timelog.txt" >>"$TMPFILE"
fi

# Insert the ""$PREVIOUS_YEAR" - vacation days left" comment if it is missing
if ! grep -q "# "$PREVIOUS_YEAR" - vacation days remaining:"  "$TIMELOGFILE"; then
    sed -i "/# "$PREVIOUS_YEAR" - vacation days:/a # "$PREVIOUS_YEAR" - vacation days remaining: 0"  "$TIMELOGFILE"
fi

# Insert the ""$CURRENT_YEAR" - vacation days" comment if it is missing
if ! grep -q "# "$CURRENT_YEAR" - vacation days:"  "$TIMELOGFILE"; then
    sed -i "/# "$PREVIOUS_YEAR" - vacation days remaining:/a # "$CURRENT_YEAR" - vacation days: 0"  "$TIMELOGFILE"
    echo "Error: Please update # "$PREVIOUS_YEAR" - vacation days in timelog.txt" >>"$TMPFILE"
fi

# Insert the ""$CURRENT_YEAR" - vacation days remaining" comment if it is missing
if ! grep -q "# "$CURRENT_YEAR" - vacation days remaining:"  "$TIMELOGFILE"; then
    sed -i "/# "$CURRENT_YEAR" - vacation days:/a # "$CURRENT_YEAR" - vacation days remaining: 0"  "$TIMELOGFILE"
fi

# Insert the "weekly working hours" comment if it is missing
if ! grep -q "# Weekly working hours:"  "$TIMELOGFILE"; then
    sed -i "/# "$CURRENT_YEAR" - vacation days remaining:/a # Weekly working hours: 0"  "$TIMELOGFILE"
fi

# Extract the username and worktime
FSFEUSER=$(grep '# Username:'  "$TIMELOGFILE" | awk '{print $3}')
WORKTIME=$(grep '# Weekly working hours:'  "$TIMELOGFILE" | awk '{print $5}')
NEXTCLOUD="/home/$USERNAME/Nextcloud/timelog_$FSFEUSER"
# Check if the target file already exists in the Nextcloud folder
if [ -d "$NEXTCLOUD" ]; then
    # Copy the timelog.txt file to the Nextcloud folder
    cp  "$TIMELOGFILE" "$NEXTCLOUD"
    # Check the exit code of the cp command
    if [ $? != 0 ]; then
        echo "Error: Copying the timelog.txt file failed (Error Code: $?)" >>"$TMPFILE"
    fi
else
    echo "Error: Please update your username in timelog.txt " >>"$TMPFILE"
fi

cd "$SCRIPTDIR"
python3 "$SCRIPTDIR"/"$SCRIPTFILE" -f  "$TIMELOGFILE" >>"$TMPFILE"

MESSAGE=$(cat "$TMPFILE")
# Check for Error Messages
if [ -n "$MESSAGE" ]; then
    $(/usr/bin/notify-send -u critical --action="xdg-open  "$TIMELOGFILE"=Open timelog.txt" "gtimelog" "$MESSAGE") >/dev/null 2>&1
    /usr/bin/notify-send -u critical "gtimelog" "$MESSAGE"
fi

rm "$TMPFILE"
