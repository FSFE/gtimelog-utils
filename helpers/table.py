# SPDX-FileCopyrightText: Copyright Thierry Husson
# SPDX-License-Identifier: CC0-1.0

# Source: https://stackoverflow.com/a/40389411

"""Functions to print a pretty table"""

def print_table(mydict, colList=None):
    """ Pretty print a list of dictionaries (myDict) as a dynamically sized table.
    If column names (colList) aren't specified, they will show in random order.
    Author: Thierry Husson - Use it as you want but don't blame me.
    """
    if not colList:
        colList = list(mydict[0].keys() if mydict else [])
    mylist = [colList] # 1st row = header
    for item in mydict:
        mylist.append(
            [str(item[col] if item[col] is not None else '') for col in colList])
    colsize = [max(map(len,col)) for col in zip(*mylist)]
    formatstr = ' | '.join(["{{:<{}}}".format(i) for i in colsize])
    mylist.insert(1, ['-' * i for i in colsize]) # Seperating line
    for item in mylist:
        print(formatstr.format(*item))
