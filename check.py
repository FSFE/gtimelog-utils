# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
A stripped down version of the analyse.py script to detect errors in timelog files

* negative time differences
* spaces used around the category devider
"""

import datetime
import sys
import os
import argparse
import configparser
import fileinput
from helpers.table import print_table
from helpers.gtimelog import (
    parse_datetime,
    format_duration_short,
    as_minutes,
    remove_tags
)

Config = configparser.ConfigParser()
Config.read("config.ini")

parser = argparse.ArgumentParser(description='Analyse gtimelogs')
parser.add_argument("-f", "--file", dest="filename", required=True,
                    help="the FILE to analyse. Use '-' to read from stdin")
parser.add_argument("-r", "--reduce-to", dest="reduce",
                    choices=["categories", "supercategories"],
                    help="Reduce activities to (super)categories")
parser.add_argument("-t", "--time-format", dest="timeformat",
                    choices=["hm", "m"], default="hm",
                    help="Set time format to HH:MM (default) or minutes")
parser.add_argument("-a", "--all", action="store_true",
                    help="Do not ignore categories set in config.ini")
args = parser.parse_args()

# Split ignored categories from config, strip them, and make them a tuple
ignored_categories = tuple(
    [x.strip() for x in Config.get("General", "ignored_categories").split(',')]
    )

errors = []

# HELPER FUNCTIONS
def get_activities_from_csv(file):
    """get valid activities from the local fsfe-activities repository"""
    activities = []
    for line in fileinput.input(file):
        activities.append(line.split("\t")[0])
    return activities


def split_activities(file):
    """Split complete timelog by lines, so every line is a separate entry"""
    items = []
    try:
        for line in fileinput.input(file):
            if ":  " in line:
                errors.append("double space after : found\n{}".format(line))
            if " :" in line:
                errors.append("space before : found\n{}".format(line))
            # Separate timestamp and actual title of the entry
            # Note: Very important that format is respected (timestamp and task
            # separated by ': ')
            time, sep, title = line.partition(': ')
            # Excludes comments, empty lines etc
            if not sep:
                continue
            try:
                time = parse_datetime(time)
            except ValueError:
                continue
            # Remove tags and trailing whitespaces from activity title
            title = remove_tags(title)

            # Modify the title based on reduction choice:
            # default: all activities in their full length, except tag (no modification)
            # categories: split at first :
            # supercategories: like categories, but then also split at first space
            if args.reduce in ("categories", "supercategories"):
                # Do not reduce OFF activities, they will be handled later
                if title.startswith("OFF"):
                    pass
                # Do not reduce slacking times, also "arrived **"
                elif '**' in title:
                    pass
                # Do not reduce ignored categories if not manually unignored with -a
                elif title.startswith(ignored_categories) and not args.all:
                    pass
                # All other, split into category, or even supercategory
                else:
                    cat, sep, _ = title.partition(':')
                    if sep:
                        title = cat
                    if args.reduce == "supercategories":
                        cat, sep, _ = title.partition(' ')
                        if sep:
                            title = cat

            items.append((time, title))

        return items
    except FileNotFoundError:
        sys.exit(f"File {file} not found.")

def format_durations(dur, total_dur, dur_format):
    """Format time depending on choice, and calculate percentage"""
    # Calculate proportion of this activity's duration in percent
    per = round(as_minutes(dur) / total_dur * 100, 2)
    # define duration format (HH:MM or minutes)
    if dur_format == "hm":
        dur = format_duration_short(dur)
    else:
        dur = as_minutes(dur)

    return dur, per


# ACTUAL WORK

# Initialize variables for Vacation Calculator
off_vacation_last_year = 0
off_vacation_this_year = 0

# Create a dictionary that lists all titles and their respective total time
# Structure: {'title': ('title', 'duration'), ...}
tasks = {}

# Initialise the current previous years
current_year = datetime.datetime.now().year
previous_year = current_year - 1

# Initialise rolling durations for off time, the ignored categories, and the
# total duration
total_duration = datetime.timedelta(0)
off_sick = off_child_sick = off_vacation = off_holiday = ignored = datetime.timedelta(0)
off_vacation_count = 0
stop = None
for item in split_activities(args.filename):
    start = stop
    stop = item[0]  # timestamp
    title = item[1] # title
    if start is None:
        start = stop

    if start > stop:
        errors.append(f"WARNING: negative duration from {start} to {stop}")

    duration = stop - start

    # skip slacking times, also arrived**
    if '**' in title:
        continue

    # check for ignored but still counted categories
    if "OFF Sick" in title:
        off_sick += duration
        continue
    if "OFF Child Sick" in title:
        off_child_sick += duration
        continue
    if "OFF Vacation" in title:
        off_vacation += duration
        if str(previous_year) in title:
            off_vacation_last_year += 1
        elif str(current_year) in title:
            off_vacation_this_year += 1
        continue
    if "OFF PublicHoliday" in title:
        off_holiday += duration
        continue
    activities = get_activities_from_csv(os.path.expanduser("~/.fsfe-activities/activities.csv"))
    if start > datetime.datetime(2025,1,1,0,0,0):
        if title.split(": ")[0] not in activities:
            print("{} is not a valid activity.".format(title.split(" ")[0]))

    # skip ignored categories defined in config.ini unless -a/--all
    if not args.all:
        if title.startswith(ignored_categories):
            ignored += duration
            continue

    # Search for entry for this activity in tasks.
    # If found, add duration. If not, create it
    if title not in tasks:
        tasks[title] = (title, duration)
    else:
        duration = tasks[title][1] + duration
        tasks[title] = (title, duration)

# check last entry
days_since = (datetime.datetime.now() - item[0]).total_seconds() / 86400.0
if days_since > 5:
    errors.append("The last entry in the gtimelog file is older then 5 days.")


###################################################################################
# Calculate and update "Remaining Vacation Days" in local timelog.txt file        #
# Compare Weekly Working Hours to Overtime/Undertime, inform user if out of range #
###################################################################################

# Initialize variables
previous_vacation_days_found = False
current_vacation_days_found = False
previous_vacations_left_found = False
current_vacations_left_found = False
previous_vacation_days_left = 0
current_vacation_days_left = 0

# Use a list to accumulate the new file content
buffer = []

with open(args.filename, 'r') as file:
    lines = file.readlines()

for line in lines:
    if line.startswith("# Weekly working hours:"):
        buffer.append(line)
        weekly_hours = int(line.split(":")[1].strip()) * 60
    elif line.startswith("# Overtime/Undertime:"):
        buffer.append(line)
        hours = int(line.split(":")[1].strip())
        minutes = int(line.split(":")[2].strip())
        if (hours < 0):
            minutes *= -1
        overtime_undertime = hours * 60 + minutes
        if (overtime_undertime >= weekly_hours or overtime_undertime <= (-weekly_hours) ):
            errors.append(f"Error: Overtime/Undertime is out of acceptable limits. Please contact your Manager")
    elif line.startswith(f"# {previous_year} - vacation days:"):
        try:
            previous_vacation_days = int(line.split(":")[1].strip())
        except ValueError:
            errors.append(f"Error: Please update '# {previous_year} - vacation days:' in timelog.txt")
            previous_vacation_days = 0
        previous_vacation_days_found = True
        buffer.append(line)
        # Calculate previous_year days left
        previous_vacation_days_left = previous_vacation_days - off_vacation_last_year
    elif line.startswith(f"# {current_year} - vacation days:"):
        try:
            current_vacation_days = int(line.split(":")[1].strip())
        except ValueError:
            errors.append(f"Error: Please update '# {current_year} - vacation days:' in timelog.txt")
            current_vacation_days = 0
        current_vacation_days_found = True
        buffer.append(line)
        # Calculate current_year days left
        current_vacation_days_left = current_vacation_days - off_vacation_this_year
    elif line.startswith(f"# {previous_year} - vacation days remaining:"):
        previous_vacations_left_found = True
        # Replace the old value with the new one
        previous_vacations_left_line = f"# {previous_year} - vacation days remaining: {previous_vacation_days_left}\n"
        buffer.append(previous_vacations_left_line)
    elif line.startswith(f"# {current_year} - vacation days remaining:"):
        current_vacations_left_found = True
        # Replace the old value with the new one
        current_vacations_left_line = f"# {current_year} - vacation days remaining: {current_vacation_days_left}\n"
        buffer.append(current_vacations_left_line)
    else:
        # For all other lines, add them to the new content
        buffer.append(line)

# If the lines for old and previous_year days were not found, add errors
if not previous_vacation_days_found:
    errors.append(f"Error: '# {previous_year} - vacation days:' entry not found in the file.")

if not current_vacation_days_found:
    errors.append(f"Error: '# {current_year} - vacation days:' entry not found in the file.")

# If the previous_year days left line was not present, add it to the end of the file
if not previous_vacations_left_found:
    errors.append(f"Error: '# {previous_year} - vacation days remaining:' entry not found in the file.")


# If the current_year days left line was not present, add it to the end of the file
if not current_vacations_left_found:
    errors.append(f"Error: '# {current_year} - vacation days remaining:' entry not found in the file.")


# Rewrite the file with the updated content
with open(args.filename, 'w') as file:
    file.writelines(buffer)

if len(errors):
    for error in errors:
        print(error)
    sys.exit(1)
