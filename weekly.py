import argparse
from helpers.gtimelog import TimeLog, Reports
import datetime
from datetime import timedelta
import fileinput
import re
import os.path

# HELPER FUNCTIONS
def get_activities_from_csv(file):
    """get valid activities from the local fsfe-activities repository"""
    activities = []
    for line in fileinput.input(file):
        activities.append(line.split("\t")[0])
    return activities

# Parse values form timelog.txt file and store as an array
# [prev_year_vacation_days, prev_year_vacation_days_remaining,
# year_vacation_days, year_vacation_days_remaining, weekly_hours]
def get_values(input_filename, year):
    values = [None, None, None, None, None]     
    prev_year = year - 1
    try:
        with open(input_filename, "r") as file:
            for line in file:
                if line.startswith(f"# {prev_year} - vacation days:"):
                    _, days = line.split(":", 1)
                    values[0] = int(days.strip())
                elif line.startswith(f"# {prev_year} - vacation days remaining:"):
                    _, days_remaining = line.split(":", 1)
                    values[1] = int(days_remaining.strip())
                elif line.startswith(f"# {year} - vacation days:"):
                    _, days = line.split(":", 1)
                    values[2] = int(days.strip())
                elif line.startswith(f"# {year} - vacation days remaining:"):
                    _, days_remaining = line.split(":", 1)
                    values[3] = int(days_remaining.strip())
                elif line.startswith("# Weekly working hours:"):
                    _, hours = line.split(":", 1)
                    values[4] = int(hours.strip())      
        # Check that the current year's vacation values are present
        if None in values[2:4]:
            raise ValueError(f"Entrie(s) '# {year} - vacation days(remaining):' not found in {input_filename}.")
        
        # Check that the weekly working hours are present
        if values[4] is None:
            raise ValueError(f"Entry '# Weekly working hours:' not found in {input_filename}.")
        
    except FileNotFoundError:
        raise ValueError(f"File {input_filename} not found.")
    
    return values

# Read overtime/undertime from file and update the local value with it
def get_overtime(input, overtime_undertime ):
    with open(input, "r") as file:
        content = file.read()
        pattern = r"Overtime/Undertime: (-?\d+) hours (\d+) min"
        match = re.search(pattern, content)
        hours = minutes = 0
        if match:
            # Extract hours and minutes
            hours_str = match.group(1) #to include the -0 usecase
            hours = int(match.group(1))
            minutes = int(match.group(2))
        if '-' in hours_str:
            minutes *= -1
        overtime_undertime += hours * 60 + minutes
    return overtime_undertime


def generate_weekly_report(name, input_filename, output_filename, year, week_number):
    virtual_midnight = datetime.time(hour=2)

    # Initialize TimeLog
    time_log = TimeLog(input_filename, virtual_midnight)

    # Initialize a list of values
    extracted_values = get_values(input_filename, year)

    # Get weekly hours
    weekly_hours = extracted_values[4] * 60

    # Calculate the start and end dates of the specified week
    start_of_week = datetime.date.fromisocalendar(year, week_number, 1)
    end_of_week = start_of_week + datetime.timedelta(days=6)
    time_window = time_log.window_for_date_range(start_of_week, end_of_week)
    email = "FSFE Council"

    # Generate the report
    report = Reports(time_window)
    with open(output_filename, "w") as file:
        report.weekly_report(file, email, name)

    activities = get_activities_from_csv(os.path.expanduser("~/.fsfe-activities/activities.csv"))

    # Read the generated report to find "Total work done this week"
    # Check the report for non valid activities and add a warning to the report
    # generated if such were used.
    with open(output_filename, "r") as file:
        content = file.read()
        pattern = r"Total work done this week: (\d+) hours(?: (\d+) min)?"
        match = re.search(pattern, content)
        hours = minutes = 0
        if match:
            # Extract hours and minutes
            hours = int(match.group(1))
            minutes = int(match.group(2)) if match.group(2) else 0

        total_work_done = hours * 60 + minutes

    # check the generated report for wrong activities
    errors_in_activities = []
    for line in fileinput.input(output_filename):
        if "Total work done this week" in line:
            break
        if year >= 2025:
            category = line.split(": ")[0].strip()
            if category==line.strip():
                category=""
            if not category in ('To', 'Subject', 'By category', 'Total work done this week'):
                if len(category) and category not in activities:
                    errors_in_activities.append(category)

    # Calculate overtime/undertime "localy"
    overtime_undertime = total_work_done - weekly_hours

    # Define path for previous week report and initial overtime text files
    directory = os.path.dirname(input_filename)
    initial_overtime = os.path.join(directory, "init_overtime.txt")
    previous_week_report = os.path.join(directory, f"{year}_W{week_number - 1:02d}_{name}.txt")

    # Get overtime/undertime value from init_overtime.txt
    if week_number == 1:
        overtime_undertime = get_overtime(initial_overtime,overtime_undertime)

    # Get overtime/undertime value from previous week report
    if os.path.isfile(previous_week_report):
        overtime_undertime = get_overtime(previous_week_report, overtime_undertime)

    overtime_undertime_hours = abs(int(overtime_undertime / 60))
    overtime_undertime_minutes = abs(overtime_undertime) % 60

    # In weekle report file after the "Subject: Weekly report" 
    # insert the overtime/undertime line and vacation info
    # add info about wrongly used activities

    buffer = []

    with open(output_filename, "r") as file:
        lines = file.readlines()

    for line in lines:
        buffer.append(line)
        # Check if this line contains "Subject: Weekly report"
        if "Subject: Weekly report" in line:
            # Add the overtime/undertime line
            if overtime_undertime < 0:
                overtime_line = f"\nOvertime/Undertime: -{overtime_undertime_hours} hours {overtime_undertime_minutes} min\n"
            else:
                overtime_line = f"\nOvertime/Undertime: {overtime_undertime_hours} hours {overtime_undertime_minutes} min\n"
            buffer.append(overtime_line)
            
            # Add vacation days and remaining vacation days information
            vacation_lines = [
                f"\n{year - 1} - vacation days: {extracted_values[0]}",
                f"\n{year - 1} - vacation days remaining: {extracted_values[1]}",
                f"\n{year} - vacation days: {extracted_values[2]}",
                f"\n{year} - vacation days remaining: {extracted_values[3]}\n",
            ]
            buffer.extend(vacation_lines)
            if len(errors_in_activities)>0:
                buffer.append("\nWRONG CATEGORIES USED: ")
                buffer.append(", ".join(errors_in_activities))


    # Write the modified content back to the file
    with open(output_filename, "w") as file:
        file.writelines(buffer)


    # In local timelog.txt file after the "# Weekly working hours:" 
    # insert the overtime/undertime line
    
    local_timelog = os.path.expanduser("~/.local/share/gtimelog/timelog.txt")
    # Check for the alternative path
    if not os.path.isfile(local_timelog):
        local_timelog = os.path.expanduser("~/.gtimelog/timelog.txt")
    
    items = []

    with open(local_timelog, "r") as file:
        lines = file.readlines()

    i = 0
    while i < len(lines):
        line = lines[i]
        items.append(line)
        
        # Check if this line contains "# Weekly working hours:"
        if "# Weekly working hours:" in line:
            if overtime_undertime < 0:
                overtime_line = f"# Overtime/Undertime: -{overtime_undertime_hours}:{overtime_undertime_minutes}\n"
            else:
                overtime_line = f"# Overtime/Undertime: {overtime_undertime_hours}:{overtime_undertime_minutes}\n"
            items.append(overtime_line)
            
            # Exclude the old overtime/undertime entry
            if lines[i + 1].startswith("# Overtime/Undertime:"):
                i += 1
        i += 1
    # Write the modified content back to the file
    with open(local_timelog, "w") as file:
        file.writelines(items)

if __name__ == "__main__":
    # Set up command line argument parsing
    parser = argparse.ArgumentParser(
        description="Generate a weekly report from gtimelog.txt file."
    )
    parser.add_argument(
        "-n", "--name", required=True, help="Staffer name to use in the report")
    parser.add_argument(
        "-i", "--input", required=True, help="Path to Staffer's timelog.txt file")
    parser.add_argument(
        "-o", "--output", required=True, help="Path to Output report file"
    )
    parser.add_argument(
        "-y", "--year", type=int, required=True, help="Year of the week for the report"
    )
    parser.add_argument(
        "-w", "--week", type=int, required=True, help="Week number for the report"
    )

    args = parser.parse_args()

    generate_weekly_report(args.name, args.input, args.output, args.year, args.week)
